package model;

import org.lightcouch.Document;

public class SharedImage extends Document{
	private String topicId;
	private long unixSeconds; //date when the image was shared
	private boolean status;
	private String hostEmail;
	
	public SharedImage(String topicID,long unixSeconds,boolean status, String hostEmail){
		this.topicId=topicID;
		this.unixSeconds=unixSeconds;
		this.status=status;
		this.hostEmail=hostEmail;
	}
	
	public String getHostEmail(){
		return hostEmail;
	}
	
	public void setHostEmail(String hostEmail){
		this.hostEmail=hostEmail;
	}
	public String getTopicId() {
		return topicId;
	}
	public void setTopicId(String topicId) {
		this.topicId = topicId;
	}
	public long getUnixSeconds() {
		return unixSeconds;
	}
	public void setUnixSeconds(long unixSeconds) {
		this.unixSeconds = unixSeconds;
	}
	public boolean isStatus() {
		return status;
	}
	public void setStatus(boolean status) {
		this.status = status;
	}

}

