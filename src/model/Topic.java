package model;

import java.util.ArrayList;

import org.lightcouch.Document;

public class Topic extends Document{
	private String hostEmail;
	private String regId;
	private ArrayList<String> tags;
	private ArrayList<String> participants=new ArrayList<String>();
	private boolean close=false;
	private String urlCollage="";
	private long dateCreation;
	
	public Topic(String hostEmail,String regId,ArrayList<String> tags,boolean close,
			String urlCollage,long dateCreation){
		this.setRegId(regId);
		this.hostEmail=hostEmail;
		this.tags=tags;
		this.close=close;
		this.urlCollage=urlCollage;
		this.dateCreation=dateCreation;
	}
	
	public Topic(String hostEmail,String regId,ArrayList<String> tags, ArrayList<String> participants, boolean close,
			String path_saved_image,long dateCreation){
		this.setRegId(regId);
		this.hostEmail=hostEmail;
		this.tags=tags;
		this.participants=participants;
		this.close=close;
		this.urlCollage=path_saved_image;
		this.dateCreation=dateCreation;
	}
	
	public String getHostEmail() {
		return hostEmail;
	}
	
	public void setHostEmail(String hostEmail) {
		this.hostEmail = hostEmail;
	}
	
	public ArrayList<String> getTags() {
		return tags;
	}
	
	public void setParticipantsRegID(ArrayList<String> regID) {
		this.participants = regID;
	}
	
	public ArrayList<String> getParticipantsRegID() {
		if(participants==null){
			participants=new ArrayList<String>();
		}
		return participants;
	}
	
	public void addParticipantRegID(String regID) {
		if(participants==null){
			participants=new ArrayList<String>();
			participants.add(regID);
		}else{
			if(!participants.contains(regID)){
				participants.add(regID);
			}
		}
	}
	
	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}
	
	public boolean isClose() {
		return close;
	}
	
	public void setClose(boolean close) {
		this.close = close;
	}
	
	public String getURLCollage() {
		return urlCollage;
	}
	
	public void setURLCollage(String path_saved_image) {
		this.urlCollage = path_saved_image;
	}

	public long getDateCreation() {
		return dateCreation;
	}

	public void setDateCreation(long dateCreation) {
		this.dateCreation = dateCreation;
	}

	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}
	
}
