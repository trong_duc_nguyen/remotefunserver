package com.root.remotefunserver;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;

import org.json.JSONException;
import org.json.JSONObject;

import database.DBactions;

public class Sockets 
{
    private ArrayList<ConnectionToClient> clientList;
    private ServerSocket serverSocket;
    public static final int SocketServerPORT = 8080;
    public static String SocketIP = "10.9.172.114";
    
    public static void main(String [] args)
    {
    	try
    	{
    		System.out.println(Sockets.getIpAddress());
    		Sockets t = new Sockets(SocketServerPORT);
    	}catch(Exception e)
    	{
    		System.out.println(e);;
    	}
    } 
    
    public Sockets(int port) 
    {
        clientList = new ArrayList<ConnectionToClient>();
       
        try
        {
        	serverSocket = new ServerSocket(port);
        }
        catch(Exception e)
        {
        	System.out.println(e);
        }

        Thread accept = new Thread(new Runnable()
        {
			@Override
			public void run()
            {
                while(true)
                {
                    try
                    {
                    	System.out.println("new connection");
                    	Socket s = serverSocket.accept();
                        try
                        {
                        	System.out.println("new connection");
                        	clientList.add(new ConnectionToClient(s, clientList.size() + 1));
                        }
                        catch(Exception e)
                        {
                        	System.out.println("Client: " + e);; 
                        }
                    }
                    catch(IOException e)
                    { 
                    	System.out.println("Init: " + e);
                    }
                }
            }
		}); 

        accept.start();
        
        //Should I create a function that dispatch the message
        //that arrive?????
    }
    
    private static String getIpAddress() {
		String ip = "";
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
					.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces
						.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface
						.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress.nextElement();

					if (inetAddress.isSiteLocalAddress()) {
						ip += "SiteLocalAddress: "
								+ inetAddress.getHostAddress() + "\n";
						//SocketIP = inetAddress.getHostAddress();
					}

				}

			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ip += "Something Wrong! " + e.toString() + "\n";
		}

		return ip;
	}

    private class ConnectionToClient
    {
    	private ObjectInputStream in;
    	private ObjectOutputStream out;
    	private Socket socket;
    	private int regId;
        
        ConnectionToClient(final Socket s, final int regId) throws IOException 
        {
            this.socket = s;
            this.regId = regId-1;
            
            Thread read = new Thread(new Runnable()
            {
    			@Override
    			public void run()
    			{
    				
                    while(!socket.isClosed())
                    {
                    	String meta="";
                        try{
                        	in = new ObjectInputStream(socket.getInputStream());
                        	
                            //receive request
                        	meta = (String)in.readObject();
                            System.out.println("Received: "+meta);
                            JSONObject jsonRequest = null;
                            String action;
                            
                            jsonRequest = new JSONObject(meta);
                            
                            if(jsonRequest.has("action")){
                            	action = jsonRequest.getString("action");
                            	processRquest(action,jsonRequest);
                            }
                        }catch (JSONException e1) {
    						// TODO Auto-generated catch block
    						e1.printStackTrace();
    					}catch (ClassNotFoundException e){
                            e.printStackTrace();
                        } catch (IOException e) {
							// TODO Auto-generated catch block
                        	e.printStackTrace();
                        	//try
                        	//{
                        		//socket.close();
                        		//clientList.remove(regId - 1);
                        	//}
                        	//catch(Exception f) {System.out.println("Removing: " + f.toString());}
                        	return;
						}
                    }
                }
    		}); 
            read.start();
        }
        
        public void processRquest(String action,JSONObject jsonRequest){
        	if( action.equalsIgnoreCase("NEW_USER") ){
        		JSONObject reply=DBactions.createUser(jsonRequest,regId);
        		
        		write(reply);
            } else if(action.equalsIgnoreCase("REQUEST_TOPICS")){
            	JSONObject reply=DBactions.requestOpenCollage();
            	
            	write(reply);
            }else if(action.equalsIgnoreCase("CREATE_COLLAGE")){
            	JSONObject reply=DBactions.createTopic(jsonRequest);
            	
            	write(reply);
            }else if(action.equalsIgnoreCase("UPDATE_CURRENT_SCREENSHOT")){
            	ArrayList<String> participants=new ArrayList<String>();
            	
            	JSONObject reply=DBactions.updateCollageImage(jsonRequest,participants);
            	write(reply);
            	//inform to users there is a new screenshot available
            	try {
					if(reply.getBoolean("isDone") && participants!=null 
							&& !participants.isEmpty()){
						notifyCollageParticipants(participants,"NOTIFY_CIMAGE_UPDATED");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }else if(action.equalsIgnoreCase("FINISH_COLLAGE")){
            	ArrayList<String> participants=new ArrayList<String>();
            	JSONObject reply=DBactions.finishTopic(jsonRequest,participants);
            	write(reply);
            	//inform to users that collage have been closed
            	try {
					if(reply.getBoolean("isDone") && participants!=null 
							&& !participants.isEmpty()){
						notifyCollageParticipants(participants,"NOTIFY_CLOSE_COLLAGE");
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }else if(action.equalsIgnoreCase("UPLOAD_OFFLINE_COLLAGE")){
            	JSONObject reply=DBactions.saveOfflineTopic(jsonRequest);
            	
            	write(reply);
            }else if(action.equalsIgnoreCase("SHARE_IMAGE")){
            	String[] ownerRegId=new String[1];
            	
            	JSONObject reply=DBactions.shareImage(jsonRequest,ownerRegId);
            	write(reply);
            	try {
					if(reply.getBoolean("isDone")){
						notifyNewImageShare(ownerRegId[0],jsonRequest.getString("image"));
					}
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            	
            }else if(action.equalsIgnoreCase("RETRIEVE_CURRENT_COLLAGE")){
            	JSONObject reply=DBactions.retriveOpenCollageFromServer(jsonRequest);
            	write(reply);
            }else if(action.equalsIgnoreCase("STOP_SHARE_IMAGE")){
            	DBactions.stopShareImage(jsonRequest);
            }else if(action.equalsIgnoreCase("REQUEST_GCOLLAGE_BYTAG")){
            	JSONObject reply=DBactions.retrieveGalleryCollage(jsonRequest);
            	write(reply);
            }else if(action.equalsIgnoreCase("CHECK_NEW_CGALLERY")){
            	JSONObject reply=DBactions.retrieveGalleryCollage(jsonRequest);
            	write(reply);
            }
        }
        
        private void write(JSONObject reply){
        	try {
        		out = new ObjectOutputStream(socket.getOutputStream());
				out.writeObject(reply.toString());
				out.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        private void notifyCollageParticipants(final ArrayList<String> participantsReg, final String action){
        	Thread notify = new Thread(new Runnable()
            {
    			@Override
    			public void run()
    			{
    				if(!socket.isClosed()){
    					for(String p : participantsReg){
    						int index = Integer.valueOf(p);
    						JSONObject notify = new JSONObject();
    						try {
								notify.put("action", action);
								clientList.get(index).write(notify);
    						} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
    					}
    					//the task has ended... return
    					return;
    				}
    			}
            });
        	notify.start();
    	}
        
        private void notifyNewImageShare(final String regId,final String image){
        	Thread notify = new Thread(new Runnable()
            {
    			@Override
    			public void run()
    			{
    				if(!socket.isClosed()){
			        	JSONObject notify = new JSONObject();
						try {
							int index=Integer.valueOf(regId);
							notify.put("action", "NOTIFY_NEW_SIMAGE");
							notify.put("image", image);
							clientList.get(index).write(notify);
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
    				}
    			}		
            });
        	notify.start();
        }
    }
}