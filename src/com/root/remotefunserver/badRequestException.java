package com.root.remotefunserver;

public class badRequestException extends Exception{
	public badRequestException(String message){
		super(message);
	}
	
	public badRequestException(){
		super("Invalid Request");
	}
}
