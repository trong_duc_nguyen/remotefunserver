package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import model.SharedImage;
import model.Topic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.lightcouch.Attachment;
import org.lightcouch.Params;
import org.lightcouch.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.root.remotefunserver.Sockets;
import com.root.remotefunserver.badRequestException;

public class DBactions {
	
	public static JSONObject createUser(JSONObject jsonRequest, int regId){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("users");
    	
    	try {
			reply.put("action", "NEW_USER");
			reply.put("isDone", false);
			
			String email = jsonRequest.getString("email");		
			if(db.getClientConnection().contains(email)==false){
            	JsonObject newUser = new JsonObject(); 
            	newUser.addProperty("_id", email);
            	newUser.addProperty("email", email);
            	newUser.addProperty("isOnline", true);
            	db.getClientConnection().save(newUser);
            	reply.remove("isDone");
            	reply.put("isDone", true);
            	reply.put("regId",regId);
			}else{
				reply.put("isDone", true);
            	reply.put("regId",regId);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

    	db.releaseConnection();
    	return reply;
	}
	
	private static String imageFromBase64(String imageString,String topicID){
		byte[] imgBytes = Base64.getDecoder().decode(imageString);  
		FileOutputStream osf;
		try {
			File imageFile = File.createTempFile("collage"+topicID, ".JPEG", new File("media/collage"));
			osf = new FileOutputStream(imageFile);
			osf.write(imgBytes);  
			osf.flush(); 
			return imageFile.getAbsolutePath();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		return "";
	}
	
	public static JSONObject createTopic(JSONObject jsonRequest){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("topics");
		
    	try {
    		reply.put("action", "CREATE_COLLAGE");
			
			if(!jsonRequest.has("hostEmail") || 
					!jsonRequest.has("regId") || !jsonRequest.has("tags")){
				throw new badRequestException();
			}
			
			String hostEmail = jsonRequest.getString("hostEmail");
			String regId = jsonRequest.getString("regId");
			
			if(hostEmail.isEmpty() || regId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			closePreviousTopicUser(hostEmail,db);
			
			JSONArray tagsArray = jsonRequest.getJSONArray("tags");
			ArrayList<String> tags = new ArrayList<String>();
			for(int i=0;i<tagsArray.length();++i){
				JSONObject tagObj = tagsArray.getJSONObject(i);
				tags.add(tagObj.getString("tag"));
			}
			
			long dateCreation=System.currentTimeMillis() / 1000L;
			Topic t = new Topic(hostEmail,regId,tags,false,"",dateCreation);
			Response resq=db.getClientConnection().save(t);
			
			reply.put("isDone", true);
        	reply.put("topicId", resq.getId());
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			// TODO Auto-generated catch block
			try {
				reply.put("isDone", false);
				reply.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
    	db.releaseConnection();
    	return reply;
	}
	
	private static void closePreviousTopicUser(String hostEmail,DBConnection db){
		
        List<Topic> listT = db.getClientConnection().view("collages/openCollage_by_hostEmail")
        							.key(hostEmail)
        							.includeDocs(true)
        							.query(Topic.class);
        
        for(Topic t : listT){
        	t.setClose(true);
        	db.getClientConnection().update(t);
        }
	}
	
	public static JSONObject saveOfflineTopic(JSONObject jsonRequest){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("topics");
		
    	try {
    		reply.put("action", "UPLOAD_OFFLINE_COLLAGE");
			
			if(!jsonRequest.has("hostEmail") || 
					!jsonRequest.has("regId") || !jsonRequest.has("tags")){
				throw new badRequestException();
			}
			
			String hostEmail = jsonRequest.getString("hostEmail");
			String regId = jsonRequest.getString("regId");
			
			if(hostEmail.isEmpty() || regId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			JSONArray tagsArray = jsonRequest.getJSONArray("tags");
			ArrayList<String> tags = new ArrayList<String>();
			for(int i=0;i<tagsArray.length();++i){
				JSONObject tagObj = tagsArray.getJSONObject(i);
				tags.add(tagObj.getString("tag"));
			}
			
			long dateCreation=System.currentTimeMillis() / 1000L;
			//collage image in base64
			String image = jsonRequest.getString("image"); 
			Attachment attachment = new Attachment(image, "image/jpeg");
			
			
			
			Topic t = new Topic(hostEmail,regId,tags,true,"",dateCreation);
			t.addAttachment("collage.jpeg", attachment);
			Response resq=db.getClientConnection().save(t);
			
			
			String topicId=resq.getId();
			
        	reply.put("isDone", true);
        	reply.put("topicId", topicId);
			
        	Topic addedTopic = db.getClientConnection().find(Topic.class,topicId);
        	addedTopic.setURLCollage("http://"+Sockets.SocketIP+":5984/topics/"+topicId+"/collage.jpeg");
        	db.getClientConnection().update(addedTopic);
        	
        	db.releaseConnection();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			// TODO Auto-generated catch block
			try {
				reply.put("isDone", false);
				reply.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
    	return reply;
	}
	
	public static JSONObject finishTopic(JSONObject jsonRequest, ArrayList<String> Participants){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("topics");
		
    	try {
    		reply.put("action", "FINISH_COLLAGE");
    		
    		if(!jsonRequest.has("hostEmail") || 
					!jsonRequest.has("regId") || !jsonRequest.has("topicId")){
				throw new badRequestException();
			}
			
    		String topicId = jsonRequest.getString("topicId");
    		String hostEmail = jsonRequest.getString("hostEmail");
    		String regId = jsonRequest.getString("regId");
			
			if(hostEmail.isEmpty() || regId.isEmpty() || topicId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			if(!db.getClientConnection().contains(topicId)){
				throw new badRequestException("Topic does not exist");
			}
    		
    		Topic topic = db.getClientConnection().find(Topic.class,topicId);

    		Participants.addAll(topic.getParticipantsRegID());
    		
			topic.setClose(true);
			
			if(jsonRequest.has("image")){
				String image = jsonRequest.getString("image"); 
				Attachment attachment = new Attachment(image, "image/jpeg");
				topic.addAttachment("collage.jpeg", attachment);
				topic.setURLCollage("http://"+Sockets.SocketIP+":5984/topics/"+topicId+"/collage.jpeg");
			}
			
			db.getClientConnection().update(topic);
			reply.put("isDone", true);

    		db.releaseConnection();
    		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			try {
				reply.put("isDone", false);
				reply.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
    	return reply;
	}
	
	public static JSONObject requestOpenCollage(){
		DBConnection db=new DBConnection("topics");
		
        //TODO: PAGGING
        List<Topic> listT = db.getClientConnection().view("collages/openCollage")
        							.includeDocs(true)
        							.query(Topic.class);
		//List<Topic> listT = db.getClientConnection().view("_all_docs").includeDocs(true).query(Topic.class);
        
        //enviar a los clientes los tags, hostEmail, topicID
        JsonArray topicsJ = new JsonArray();
        for(Topic t : listT){
        	JsonObject tJson = new JsonObject();
        	tJson.addProperty("topicId", t.getId());
        	tJson.addProperty("hostEmail", t.getHostEmail());
        	JsonArray tagsJ = new JsonArray();
        	for(String tag : t.getTags()){
        		JsonObject tagJson = new JsonObject();
    			tagJson.addProperty("tag", tag);
    			tagsJ.add(tagJson);
        	}
        	tJson.add("tags", tagsJ);
        	Topic topic = db.getClientConnection().find(Topic.class,t.getId(),
    				new Params().attachments());
        	String collageBase64="";
			if(topic.getAttachments()!=null){
				if(topic.getAttachments().containsKey("collage.jpeg")){
					collageBase64 = topic.getAttachments().get("collage.jpeg").getData();
				}
			}
			tJson.addProperty("image", collageBase64);
			System.out.println(collageBase64);
        	topicsJ.add(tJson);
        }
        
        db.releaseConnection();
        
        JSONObject reply = new JSONObject();               	
		try {
			reply.put("action", "REQUEST_TOPICS");
			reply.put("result",topicsJ.toString());
			reply.put("isDone",true);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return reply;
	}
	
	public static JSONObject updateCollageImage(JSONObject jsonRequest, ArrayList<String> particitpants){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("topics");
		
    	try {
    		reply.put("action", "UPDATE_CURRENT_SCREENSHOT");
    		if(!jsonRequest.has("hostEmail") || 
					!jsonRequest.has("regId") || !jsonRequest.has("topicId") ||
					!jsonRequest.has("image") ){
				throw new badRequestException();
			}
			
    		String topicId = jsonRequest.getString("topicId");
    		String hostEmail = jsonRequest.getString("hostEmail");
    		String regId = jsonRequest.getString("regId");
			
			if(hostEmail.isEmpty() || regId.isEmpty() || topicId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			if(!db.getClientConnection().contains(topicId)){
				throw new badRequestException("Topic does not exist");
			}
    		
    		Topic topic = db.getClientConnection().find(Topic.class,topicId);
			//copy list participants in the topic to future nitifications
			particitpants.addAll(topic.getParticipantsRegID());
			
			String image = jsonRequest.getString("image"); 
			Attachment attachment = new Attachment(image, "image/jpeg");
			
			topic.addAttachment("collage.jpeg", attachment);
			
			topic.setURLCollage("http://"+Sockets.SocketIP+":5984/topics/"+topicId+"/collage.jpeg");
			
			db.getClientConnection().update(topic);
			reply.put("isDone", true);
			db.releaseConnection();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			// TODO Auto-generated catch block
			try {
				reply.put("isDone", false);
				reply.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
    	return reply;
	}
	
	public static JSONObject shareImage(JSONObject jsonRequest,String[] OwnerRegId){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("share_images");
		DBConnection dbt=new DBConnection("topics");
		
		try {
			reply.put("action", "SHARE_IMAGE");
			
			if(!jsonRequest.has("hostEmail") || !jsonRequest.has("regId") 
					|| !jsonRequest.has("topicId") || !jsonRequest.has("image")){
				throw new badRequestException();
			}
			
    		String topicId = jsonRequest.getString("topicId");
    		String hostEmail = jsonRequest.getString("hostEmail");
    		String regId = jsonRequest.getString("regId");
    		String image = jsonRequest.getString("image"); 
			
			if(hostEmail.isEmpty() || regId.isEmpty() || topicId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			if(!dbt.getClientConnection().contains(topicId)){
				throw new badRequestException("Topic does not exist");
			}

			Topic topic = dbt.getClientConnection().find(Topic.class,topicId,
    				new Params().attachments());
			
			if(topic.isClose()){
				throw new badRequestException("Topic has been closed");
			}
    		
			OwnerRegId[0]=topic.getRegId(); 
					
			//topic exist???
			long dateUnix=System.currentTimeMillis() / 1000L;
			Attachment attachment = new Attachment(image, "image/jpeg");
			SharedImage sI=new SharedImage(topicId, dateUnix, false, hostEmail);
			sI.addAttachment("share.jpeg", attachment);
			
			db.getClientConnection().save(sI);
			
			reply.put("isDone",true);
			reply.put("topicId", topicId);
			
			db.releaseConnection();
			dbt.releaseConnection();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			// TODO Auto-generated catch block
			try {
				reply.put("isDone", false);
				reply.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		return reply;
	}
	
	public static void stopShareImage(JSONObject jsonRequest){
		
		DBConnection dbt=new DBConnection("topics");
		
		try {
			
			if(!jsonRequest.has("hostEmail") || !jsonRequest.has("regId") 
					|| !jsonRequest.has("topicId") ){
				throw new badRequestException();
			}
			
    		String topicId = jsonRequest.getString("topicId");
    		String hostEmail = jsonRequest.getString("hostEmail");
    		String regId = jsonRequest.getString("regId");
			
			if(hostEmail.isEmpty() || regId.isEmpty() || topicId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			if(!dbt.getClientConnection().contains(topicId)){
				throw new badRequestException("Topic does not exist");
			}

			Topic topic = dbt.getClientConnection().find(Topic.class,topicId,
    				new Params().attachments());
			
			if(!topic.isClose()){
				ArrayList<String> Participants = topic.getParticipantsRegID();
				
				//delete from participantList the user
				int numP=Participants.size();
				for(int i=0; i<numP ;i++){
					if(Participants.get(i).equals(regId)){
						Participants.remove(i);
						break;
					}
				}
				topic.setParticipantsRegID(Participants);
				
				dbt.getClientConnection().update(topic);
				dbt.releaseConnection();
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static JSONObject retriveOpenCollageFromServer(JSONObject jsonRequest){
		JSONObject reply = new JSONObject();
    	
		DBConnection db=new DBConnection("topics");
		
		try {
			reply.put("action", "RETRIEVE_CURRENT_COLLAGE");
			
			if(!jsonRequest.has("hostEmail") || !jsonRequest.has("regId") 
					|| !jsonRequest.has("topicId")){
				throw new badRequestException();
			}
			
    		String topicId = jsonRequest.getString("topicId");
    		String hostEmail = jsonRequest.getString("hostEmail");
    		String regId = jsonRequest.getString("regId");
			
			if(hostEmail.isEmpty() || regId.isEmpty() || topicId.isEmpty()){
				throw new badRequestException("User is not registered");
			}
			
			if(!db.getClientConnection().contains(topicId)){
				throw new badRequestException("Topic does not exist");
			}

			Topic topic = db.getClientConnection().find(Topic.class,topicId,
    				new Params().attachments());
			
			if(topic.isClose()){
				throw new badRequestException("Topic has been closed");
			}
			
			//check is first time and user need to register to get 
			//update of current collage
			boolean register = jsonRequest.getBoolean("register");
			if(register){
				topic.addParticipantRegID(regId);
				db.getClientConnection().update(topic);
			}
			
			String collageBase64="";
			if(topic.getAttachments()!=null){
				if(topic.getAttachments().containsKey("collage.jpeg")){
					collageBase64 = topic.getAttachments().get("collage.jpeg").getData();
				}
			}
			
			reply.put("image", collageBase64);
			reply.put("isDone", true);
			db.releaseConnection();
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (badRequestException e) {
			// TODO Auto-generated catch block
			try {
				reply.put("isDone", false);
				reply.put("error",e.getMessage());
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
    	return reply;
	}
	
	public static JSONObject retrieveGalleryCollage(JSONObject jsonRequest){
		DBConnection db=new DBConnection("topics");
		int resultsPerPage=25;
		long startKey=-1;
		
		if(jsonRequest.has("limit")){
			try {
				resultsPerPage=jsonRequest.getInt("limit");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		if(jsonRequest.has("startKey")){
			try {
				startKey=jsonRequest.getLong("startKey");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		boolean descending=true;
		if(jsonRequest.has("descending")){
			try {
				descending=jsonRequest.getBoolean("descending");
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		List<Topic> listT = new ArrayList<Topic>();
        //TODO: PAGGING
		if(startKey!=-1){
			listT = db.getClientConnection().view("collages/collage_galery")
        							.includeDocs(true)
        							.startKey(startKey)
        							.limit(resultsPerPage+1)
        							.descending(descending)
        							.query(Topic.class);
		}else{
			listT = db.getClientConnection().view("collages/collage_galery")
									.includeDocs(true)
									.limit(resultsPerPage+1)
									.descending(descending)
									.query(Topic.class);
		}
		
        //enviar a los clientes los tags, hostEmail, topicID
        JsonArray topicsJ = new JsonArray();
        for(Topic t : listT){
        	JsonObject tJson = new JsonObject();
        	tJson.addProperty("topicId", t.getId());
        	tJson.addProperty("hostEmail", t.getHostEmail());
        	tJson.addProperty("urlCollage", t.getURLCollage());
        	tJson.addProperty("dateCreation", t.getDateCreation());
        	JsonArray tagsJ = new JsonArray();
        	for(String tag : t.getTags()){
        		JsonObject tagJson = new JsonObject();
    			tagJson.addProperty("tag", tag);
    			tagsJ.add(tagJson);
        	}
        	tJson.add("tags", tagsJ);
        	topicsJ.add(tJson);
        }
        
        db.releaseConnection();
        
        JSONObject reply = new JSONObject();               	
		try {
			reply.put("action", jsonRequest.getString("action"));
			reply.put("result",topicsJ.toString());
			reply.put("isDone",true);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        return reply;
	}

}
