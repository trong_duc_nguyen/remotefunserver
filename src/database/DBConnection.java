package database;

import java.util.List;

import org.lightcouch.CouchDbClient;

import com.google.gson.JsonObject;

public class DBConnection {
	private CouchDbClient dbClient;
	private boolean createDbIfNotExist=true;
	private String protocol="http";
	private String host="localhost";
	private int port=5984;
	private String errMsg;
	
	public DBConnection(String databaseName){
		dbClient=new CouchDbClient(databaseName, createDbIfNotExist, protocol, host, port, null, null);
	}
	
	public DBConnection(String databaseName,boolean createDbIfNotExist,
			String protocol, String host, int port){
		dbClient=new CouchDbClient(databaseName, createDbIfNotExist, protocol, host, port, null, null);
	}
	
	public void releaseConnection(){
		if(dbClient!=null){
			dbClient.shutdown();
		}
	}
	
	public CouchDbClient getClientConnection(){
		return dbClient;
	}
	
	public boolean insertNewRow(JsonObject json){
		boolean result=false;
		dbClient.save(json);
		return result;
	}
	
	public boolean updateRow(){
		boolean result=false;
		return result;
	}
}
